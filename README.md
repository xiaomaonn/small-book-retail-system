# small-book-retail-system

## Implement a small Book Retail system, here are requirements:
1. System can create member with three different member types (Gold, Silver, Coper)

2. System can submit order
   * Need to calculate the member’s point. Here is formula.
   * Gold member – Total price of order * 3
   * Silver member –Total price of order * 2
   * Coper member – Total price of order
   * System can query orders

Expectations:
 * Required
 * Implements 3 Rest APIs (H2 as DB)
 * Unit Test
 * Integration Test
 * Design exception and exception handler

 * Log API request and response
 * All tests need to be passed
 * Startup successfully

3. Nice to have in the code (Optional)
 * Design pattern
 * Maintainability


