package com.maomao.book.service;

import com.maomao.book.constant.CommonConstant;
import com.maomao.book.domain.Member;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class MemberServiceTest {

    @Autowired
    private MemberService memberService;

    @Test
    public void testSave() {
        Member member = new Member();
//        member.setId(8L);
        member.setName("James");
        member.setType(CommonConstant.MEMBER_TYPE_GOLD);
        member.setPoint(0);
        Member savedMember = memberService.saveMember(member);
        Assert.assertNotNull(savedMember.getId());
    }

    @Test
    public void testGetMemberPage() {
        Page<Member> memberPage = memberService.getMemberPage(0, 1);
        Assert.assertFalse(0 == memberPage.getTotalPages());
    }

}
