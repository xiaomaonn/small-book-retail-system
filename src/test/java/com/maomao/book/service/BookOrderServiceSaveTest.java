package com.maomao.book.service;

import com.maomao.book.domain.BookOrder;
import com.maomao.book.domain.Member;
import com.maomao.book.repository.MemberRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class BookOrderServiceSaveTest {

    @Autowired
    private BookOrderService bookOrderService;

    @Autowired
    private MemberRepository memberRepository;

    /**
     * assert the runtime exception will throw.
     */
    @Test(expected = RuntimeException.class)
    public void testSaveFailed() {
        BookOrder bookOrder = new BookOrder();
        bookOrder.setTotalPrice(500.);
        bookOrder.setMemberId(12L);
        bookOrderService.saveBookOrder(bookOrder);
    }

    @Test
    public void testSaveGold() {
        BookOrder bookOrder = new BookOrder();
        bookOrder.setTotalPrice(500.);
        bookOrder.setMemberId(1L);
        bookOrderService.saveBookOrder(bookOrder);
        Member member = memberRepository.findById(1L).get();
        Assert.assertEquals(member.getPoint(), 1500);
    }

    @Test
    public void testSaveSilver() {
        BookOrder bookOrder = new BookOrder();
        bookOrder.setTotalPrice(500.);
        bookOrder.setMemberId(2L);
        bookOrderService.saveBookOrder(bookOrder);
        Member member = memberRepository.findById(2L).get();
        Assert.assertEquals(member.getPoint(), 1000);
    }


    @Test
    public void testSaveCoper() {
        BookOrder bookOrder = new BookOrder();
        bookOrder.setTotalPrice(500.);
        bookOrder.setMemberId(3L);
        bookOrderService.saveBookOrder(bookOrder);
        Member member = memberRepository.findById(3L).get();
        Assert.assertEquals(member.getPoint(), 500);
    }
}
