package com.maomao.book.controller;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.maomao.book.BookAppStartup;
import com.maomao.book.domain.BookOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * @author mao.kaiyin
 * @date 2022-04-20
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookAppStartup.class)
@AutoConfigureMockMvc
public class SaveBookOrderTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testSaveBookOrderFailed() throws Exception {
        BookOrder bookOrder = new BookOrder();
        bookOrder.setMemberId(125L); // The member ID not in the database.
        bookOrder.setTotalPrice(120.0);
        JsonMapper jsonMapper = new JsonMapper();
        String content = jsonMapper.writeValueAsString(bookOrder);

        mockMvc.perform(MockMvcRequestBuilders.post("/order/save").contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("failed"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("Do not find the member with id:125"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }

    @Test
    public void testSaveBookOrderSuccess() throws Exception {
        BookOrder bookOrder = new BookOrder();
        bookOrder.setMemberId(4L); // The member ID in the database.
        bookOrder.setTotalPrice(120.0);
        JsonMapper jsonMapper = new JsonMapper();
        String content = jsonMapper.writeValueAsString(bookOrder);

        mockMvc.perform(MockMvcRequestBuilders.post("/order/save").contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").value("success"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.id").isNotEmpty())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}
