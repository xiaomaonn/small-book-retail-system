package com.maomao.book.controller;

import com.maomao.book.BookAppStartup;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * @author mao.kaiyin
 * @date 2022-04-20
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookAppStartup.class)
@AutoConfigureMockMvc
public class QueryBookOrderTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testQueryMember() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/order/page/0/10"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.status").value("success"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }


}
