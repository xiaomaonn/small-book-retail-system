package com.maomao.book.controller;

import com.fasterxml.jackson.databind.json.JsonMapper;
import com.maomao.book.BookAppStartup;
import com.maomao.book.constant.CommonConstant;
import com.maomao.book.domain.Member;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author mao.kaiyin
 * @date 2022-04-20
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookAppStartup.class)
@AutoConfigureMockMvc
public class SaveMemberTest {

    @Autowired
    private MockMvc mockMvc;


    @Test
    public void testSaveMember() throws Exception {
        JsonMapper jsonMapper = new JsonMapper();
        Member member = new Member();
        member.setName("Jone");
        member.setType(CommonConstant.MEMBER_TYPE_GOLD);
        member.setPoint(0);
        String content = jsonMapper.writeValueAsString(member);
        mockMvc.perform(MockMvcRequestBuilders.post("/member/save").contentType(MediaType.APPLICATION_JSON).content(content))
                .andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$.status").value("success"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }


}
