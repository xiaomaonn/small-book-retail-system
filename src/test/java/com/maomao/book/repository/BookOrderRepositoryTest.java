package com.maomao.book.repository;


import com.maomao.book.domain.BookOrder;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class BookOrderRepositoryTest {

    @Autowired
    private BookOrderRepository bookOrderRepository;

    @Test
    public void saveTest() throws Exception {
        BookOrder bookOrder = new BookOrder();
        bookOrder.setMemberId(1L);
        bookOrder.setTotalPrice(120.);
        bookOrderRepository.save(bookOrder);
    }
}
