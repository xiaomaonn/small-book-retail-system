package com.maomao.book.repository;


import com.maomao.book.constant.CommonConstant;
import com.maomao.book.domain.Member;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class MemberRepositoryTest {

    @Autowired
    private MemberRepository memberRepository;

    @Test
    public void saveTest() throws Exception {
        Member member = new Member();
        member.setName("James");
        member.setType(CommonConstant.MEMBER_TYPE_GOLD);
        member.setPoint(0);
        Member savedMember = memberRepository.save(member);
        Assert.assertNotNull(savedMember.getId());
    }
}
