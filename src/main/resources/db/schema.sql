DROP table if exists t_member;

CREATE TABLE t_member
(
    id    BIGINT       NOT NULL AUTO_INCREMENT,
    name  VARCHAR(100) NOT NULL,
    type  INT          NOT NULL,
    point INT DEFAULT 0,
    PRIMARY KEY (id)
);

DROP TABLE if exists T_ORDER ;
CREATE TABLE T_ORDER
(
    id          BIGINT NOT NULL AUTO_INCREMENT,
    member_id   BIGINT,
    total_price DOUBLE,
    PRIMARY KEY (id)
)