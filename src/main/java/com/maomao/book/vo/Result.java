package com.maomao.book.vo;

import lombok.Data;

/**
 * rest response entry
 *
 * @author kevin.mao
 * @date 2022/4/19
 */
@Data
public class Result<T> {

    private static final String SUCCESS = "success";

    private static final String FAILED = "failed";


    private String status;
    private String message;

    private T data;

    /**
     * success response
     *
     * @return the success result
     */
    public static Result success() {
        return new Result().status(SUCCESS);
    }

    /**
     * with data success response
     *
     * @param data response data
     * @return the success response
     */
    public static <T> Result<T> success(T data) {
        return new Result<>().status(SUCCESS).data(data);
    }

    /**
     * the failed response with error description
     *
     * @param message error description
     * @return the failed response
     */
    public static Result failed(String message) {
        return new Result().message(message).status(FAILED);
    }

    /**
     * set the response status
     *
     * @param status status
     * @return response result
     */
    public Result status(String status) {
        this.status = status;
        return this;
    }

    /**
     * set the response data
     *
     * @param data response data
     * @return response result
     */
    public Result data(T data) {
        this.data = data;
        return this;
    }


    /**
     * set the response description
     *
     * @param message tips message
     * @return response result
     */
    public Result message(String message) {
        this.message = message;
        return this;
    }

}
