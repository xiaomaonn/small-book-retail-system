package com.maomao.book.service.impl;

import com.maomao.book.domain.Member;
import com.maomao.book.repository.MemberRepository;
import com.maomao.book.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * The service class of member
 *
 * @author kevin.mao
 */
@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberRepository memberRepository;

    /**
     * save the member information
     *
     * @param member book retail system member
     */
    public Member saveMember(Member member) {
        return memberRepository.save(member);
    }

    @Override
    public Page<Member> getMemberPage(int page, int size) {
        PageRequest pageable = PageRequest.of(page, size, Sort.Direction.DESC, "id");
        return memberRepository.findAll(pageable);
    }

}
