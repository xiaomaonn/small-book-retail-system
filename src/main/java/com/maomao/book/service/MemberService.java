package com.maomao.book.service;

import com.maomao.book.domain.Member;
import org.springframework.data.domain.Page;

/**
 * do some service of member
 *
 * @author mao.kaiyin
 */
public interface MemberService {

    /**
     * save the member information
     *
     * @param member book retail system member
     */
    Member saveMember(Member member);

    /**
     * select number list with page
     *
     * @param page page number
     * @param size the max items of one page
     * @return page of member
     */
    Page<Member> getMemberPage(int page, int size);
}
