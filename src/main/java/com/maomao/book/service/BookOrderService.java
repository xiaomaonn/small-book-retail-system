package com.maomao.book.service;

import com.maomao.book.domain.BookOrder;
import org.springframework.data.domain.Page;

/**
 * the service interface of book order
 *
 * @author kevin.mao
 */
public interface BookOrderService {

    /**
     * save book order information
     *
     * @param bookOrder book order information
     */
    BookOrder saveBookOrder(BookOrder bookOrder);


    /**
     * select page book orders
     *
     * @param page the page number
     * @param size the max  items in one page
     * @return order page
     */
    Page<BookOrder> getBookOrderPage(int page, int size);
}
