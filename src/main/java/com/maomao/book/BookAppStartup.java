package com.maomao.book;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class BookAppStartup {

    public static void main(String[] args) {
        SpringApplication.run(BookAppStartup.class, args);
    }

}
