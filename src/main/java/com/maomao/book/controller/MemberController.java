package com.maomao.book.controller;

import com.maomao.book.domain.Member;
import com.maomao.book.service.MemberService;
import com.maomao.book.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/member")
public class MemberController {

    @Autowired
    private MemberService memberService;

    @PostMapping("/save")
    public Result save(@RequestBody Member member) {
        memberService.saveMember(member);
        return Result.success();
    }


    @GetMapping("/page/{page}/{size}")
    public Result<Page<Member>> findMemberPage(@PathVariable("page") int page, @PathVariable("size") int size) {
        return Result.success(memberService.getMemberPage(page, size));
    }

}
