package com.maomao.book.controller;

import com.maomao.book.domain.BookOrder;
import com.maomao.book.domain.Member;
import com.maomao.book.service.BookOrderService;
import com.maomao.book.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class BookOrderController {

    @Autowired
    private BookOrderService bookOrderService;

    @PostMapping("/save")
    public Result<BookOrder> save(@RequestBody BookOrder bookOrder) {
        return Result.success(bookOrderService.saveBookOrder(bookOrder));
    }

    @GetMapping("/page/{page}/{size}")
    public Result<Page<BookOrder>> findMemberPage(@PathVariable("page") int page, @PathVariable("size") int size) {
        return Result.success(bookOrderService.getBookOrderPage(page, size));
    }
}
