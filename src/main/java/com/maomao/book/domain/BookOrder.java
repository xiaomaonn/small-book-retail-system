package com.maomao.book.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "t_order")
@Data
public class BookOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long memberId;

    private Double totalPrice;

}
