package com.maomao.book.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "t_member")
@Data
public class Member {

    /**
     * because there are four members are in the database, the initial value is 5
     */
    @Id
    @Column(nullable = false)
    @GeneratedValue(generator = "member_sequence")
    @SequenceGenerator(name = "member_sequence", sequenceName = "member_sequence", allocationSize = 20, initialValue = 5)
    private Long id;

    private String name;

    private int type;


    private int point;

}