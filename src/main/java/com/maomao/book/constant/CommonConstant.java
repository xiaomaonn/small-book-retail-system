package com.maomao.book.constant;

/**
 * common constant
 *
 * @author kevin.mao
 */
public interface CommonConstant {

    int MEMBER_TYPE_GOLD = 1;

    int MEMBER_TYPE_SILVER = 2;

    int MEMBER_TYPE_COPER = 3;

}
