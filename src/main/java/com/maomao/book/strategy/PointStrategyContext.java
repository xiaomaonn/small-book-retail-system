package com.maomao.book.strategy;

public class PointStrategyContext {

    private PointStrategy pointStrategy;

    public PointStrategyContext(PointStrategy pointStrategy) {
        this.pointStrategy = pointStrategy;
    }

    public int calcPoint(double price) {
        return this.pointStrategy.calcPoint(price);
    }

}
