package com.maomao.book.strategy.impl;

import com.maomao.book.strategy.PointStrategy;

/**
 * calculate the gold member’s point.
 */
public class GoldMemberPointStrategy implements PointStrategy {

    @Override
    public int calcPoint(double price) {
        return (int) (price * 3);
    }

}
