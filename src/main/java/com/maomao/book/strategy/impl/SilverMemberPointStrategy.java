package com.maomao.book.strategy.impl;

import com.maomao.book.strategy.PointStrategy;

/**
 * calculate the Silver member’s point.
 */
public class SilverMemberPointStrategy implements PointStrategy {

    @Override
    public int calcPoint(double price) {
        return (int) (price * 2);
    }

}
