package com.maomao.book.strategy.impl;

import com.maomao.book.strategy.PointStrategy;

/**
 * calculate the Coper member’s point.
 */
public class CoperMemberPointStrategy implements PointStrategy {

    @Override
    public int calcPoint(double price) {
        return (int) (price);
    }

}
