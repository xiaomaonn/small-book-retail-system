package com.maomao.book.strategy;

import com.maomao.book.constant.CommonConstant;
import com.maomao.book.domain.Member;
import com.maomao.book.strategy.impl.CoperMemberPointStrategy;
import com.maomao.book.strategy.impl.GoldMemberPointStrategy;
import com.maomao.book.strategy.impl.SilverMemberPointStrategy;

import java.util.HashMap;
import java.util.Map;

/**
 * Point Strategy Builder
 *
 * @author kevin.mao
 */
public class PointStrategyBuilder {

    private static Map<Integer, PointStrategy> strategyMap = new HashMap() {{
        put(CommonConstant.MEMBER_TYPE_GOLD, new GoldMemberPointStrategy());
        put(CommonConstant.MEMBER_TYPE_SILVER, new SilverMemberPointStrategy());
        put(CommonConstant.MEMBER_TYPE_COPER, new CoperMemberPointStrategy());
    }};

    public PointStrategy build(Member member) {
        PointStrategy pointStrategy = strategyMap.get(member.getType());
        if (pointStrategy == null) {
            pointStrategy = new CoperMemberPointStrategy();
        }
        return pointStrategy;
    }

}
