package com.maomao.book.strategy;

/**
 * calculate member point
 */
public interface PointStrategy {

    /**
     * calculate the point from price
     *
     * @param price order price
     * @return the point
     */
    int calcPoint(double price);
}
