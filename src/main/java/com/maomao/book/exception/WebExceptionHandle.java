package com.maomao.book.exception;

import com.maomao.book.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常拦截
 *
 * @author mao.kaiyin
 */
@ControllerAdvice
@ResponseBody
@Slf4j
public class WebExceptionHandle {

    /**
     * 拦截所有异常
     *
     * @param e 异常信息
     * @return 响应结果
     */
    @ExceptionHandler(Exception.class)
    public Result handleHttpMessageNotReadableException(Exception e) {
        log.error("Exception intercepted, message:{}", e.getMessage());
        return Result.failed(e.getMessage());
    }

}
